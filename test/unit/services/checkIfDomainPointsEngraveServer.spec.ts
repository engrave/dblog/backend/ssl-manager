import { describe } from 'mocha';
import { expect } from 'chai';
import checkIfDomainPointsEngraveServer from '../../../src/services/checkIfDomainPointsEngraveServer';

describe("checkIfDomainPointsEngraveServer", () => {

    let env:any = null;

    before(() => {
        env = process.env;
        process.env = { SERVER_IP: '167.71.119.171' }
    })

    after( () => {
        process.env = env;
    })

    it("Should validate engrave.website as true", async () => {
        const isPointing =  await checkIfDomainPointsEngraveServer('engrave.website');
        expect(isPointing).to.be.true;
    })

    it("Should validate engrave.site as true", async () => {
        const isPointing =  await checkIfDomainPointsEngraveServer('engrave.site');
        expect(isPointing).to.be.true;
    })

    it("Should validate example.com as false", async () => {
        const isPointing =  await checkIfDomainPointsEngraveServer('example.com');
        expect(isPointing).to.be.false;
    })

})