import * as dns from 'dns';
import { logger } from "../submodules/shared-library/utils";

export default async (domain: string) => {
    return new Promise ( (resolve) => {
        dns.lookup(domain, null, (error, address) => {
            if(error) resolve(false);
            else {
                logger.info({domain, address}, `Record A for ${domain} is pointing to: ${address}`);
                resolve(address == process.env.SERVER_IP);
            }
        });
    })
}
