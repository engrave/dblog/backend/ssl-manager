import { handleServiceError, microservices } from "../../submodules/shared-library";
import axios from 'axios';

export default async (custom_domain: string) => {

    return handleServiceError(async () => {

       const options = {
            method: 'POST',
            data: {
                domain: custom_domain
            },
            url: `http://${microservices.nginx_configurator}/configuration/generate`
        };
    
        return await axios(options);

    })
    
} 