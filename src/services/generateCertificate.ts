import {logger} from "../submodules/shared-library/utils";

/**
 * Directory URLs
 */

const directory = {
    buypass: {
        staging: 'https://api.test4.buypass.no/acme/directory',
        production: 'https://api.buypass.com/acme/directory'
    },
    letsencrypt: {
        staging: 'https://acme-staging-v02.api.letsencrypt.org/directory',
        production: 'https://acme-v02.api.letsencrypt.org/directory'
    },
    zerossl: {
        production: 'https://acme.zerossl.com/v2/DV90'
    }
};

const leStore = require('le-store-certbot').create({
    configDir: process.env.SSL_CERTIFICATES_DIR,                   // or /etc/acme or wherever
    privkeyPath: ':configDir/live/:hostname/privkey.pem',          //
    fullchainPath: ':configDir/live/:hostname/fullchain.pem',      // Note: both that :configDir and :hostname
    certPath: ':configDir/live/:hostname/cert.pem',                //       will be templated as expected by
    chainPath: ':configDir/live/:hostname/chain.pem',              //       greenlock.js
    logsDir: process.env.SSL_CERTIFICATES_DIR + '/engrave-logs',
    webrootPath: '/app/certbot/.well-known/acme-challenge',
    debug: true
});

export default async (domain: string) => {
    try {
        logger.info(` * Trying to generate certificates for ${domain}`);

        const opts = { domains: [domain, `www.${domain}`], email: process.env.SSL_EMAIL, agreeTos: true, communityMember: false };
        const greenlock = require('greenlock').create({ version: 'draft-12', server: directory.letsencrypt.production, store: leStore });

        await greenlock.register(opts);

        logger.info(" * SSL certificates generated successfully!");
    } catch (error) {
        logger.error(error, "SSL error at generateCertificate");
        throw error;
    }
};
