import { Blogs } from "../../submodules/shared-library/models/Blogs";
import generateCertificate from "../../services/generateCertificate";
import checkIfDomainPointsEngraveServer from "../../services/checkIfDomainPointsEngraveServer";
import generateNginxSettings from "../../services/nginx/generateNginxSettings";
import {logger} from "../../submodules/shared-library/utils";

    async function regenerateAll () {
    
    try {
        const blogs = await Blogs.find( { custom_domain: { $ne: null } });
        
        for(const blog of blogs) {
            try {
                const { custom_domain } = blog;
                if (await checkIfDomainPointsEngraveServer(custom_domain) && await checkIfDomainPointsEngraveServer(`www.${custom_domain}`)) {
                        logger.info(" * Generate NGINX config before generating SSL", custom_domain);
                        await generateNginxSettings(custom_domain);
                        logger.info(" * Generate SSL", custom_domain);
                        await generateCertificate(custom_domain);
                        logger.info(" * Generate new NGINX config", custom_domain);
                        await generateNginxSettings(custom_domain);
                        logger.info(" * NGINX with SSL generated for: ", custom_domain);
                }
            } catch (error) {
                logger.error(error, "Generating SSL error");
            }
        }
    } catch (error) {
        logger.error(error, "Regenerating SSL error");
    }
}

export default regenerateAll;