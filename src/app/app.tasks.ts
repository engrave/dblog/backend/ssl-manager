import { CronJob } from 'cron';
import regenerateAll from '../tasks/certificates/regenerateAll';
import { logger } from '../submodules/shared-library';

function tasks() {
    logger.info(" * SSL module initialized");

    new CronJob('*/5 * * * *', regenerateAll, null, true, 'America/Los_Angeles');
}

export default tasks;