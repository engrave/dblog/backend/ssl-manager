import healthApi from "../routes/health/health.routes";
import sslApi from "../routes/ssl/ssl.routes";
import domainApi from "../routes/domain/domain.routes";
import {endpointLogger} from "../submodules/shared-library/utils/logger";

function routes(app:any) {
    app.use('/health', healthApi);
    app.use('/ssl', endpointLogger, sslApi);
    app.use('/domain', endpointLogger, domainApi);
}

export default routes;
