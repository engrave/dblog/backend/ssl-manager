import * as express from "express";
import validate from "./routes/validate";
import generate from "./routes/generate";

const sslApi: express.Router = express.Router();

sslApi.post('/validate', validate.middleware, validate.handler);
sslApi.post('/generate', generate.middleware, generate.handler);

export default sslApi;