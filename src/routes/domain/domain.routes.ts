import * as express from "express";
import validate from "./routes/validate";

const domainsApi: express.Router = express.Router();

domainsApi.post('/validate', validate.middleware, validate.handler);

export default domainsApi;