import app from './app/app';
import { listenOnPort } from './submodules/shared-library/utils/listenOnPort';
import * as mongoose from 'mongoose';
import { mongo, logger } from './submodules/shared-library';

( async () => {

    try {
        
        await mongoose.connect(mongo.uri, mongo.options);
    
        listenOnPort(app, 3000);

    } catch (error) {
        logger.error('Error encountered while starting the application: ', error.message);
        process.exit(1);
    }
})();
